# Parcial 4: Buildroot.
## Proceso de creación.

En la terminal, dentro de la carpeta de Buildroor:
  
 - Se usó la configuración estándar para la arquiectura x86_64, por lo que se ejecutó:

    ```sh
    $ make pc_x86_64_bios_defconfig
    ```
   
 - Dado el punto anterior se pasó a la configuración con:

    ```sh
    $ make menuconfig
    ```
   Ya que se usó la configuración de arquitectura estándar sólo se agregaron los paquetes y bibliotecas al build, además de aumentar el tamaño de la imagen.
   
 - Una vez configurado el buil sólo se lleva a cabo el compilado con:

    ```sh
    $ make
    ```
    
    este proceso llevó aproximadamente 3 horas.
  - Terminado el proceso de compilado se tiene un archivo ***disk.img***. Dado que **VirtualBox** no soporta ese formato se ejecutó 
 
    ```sh
    $ VBoxManage convertdd disk.img parcial4.vdi
    ```
    

En **VirtualBox**:

 - Se creó una nueva máquina virtual y se seleccionó el archivo ***parcial4.vdi*** como disco virtual.

 - Al ejecutar la máquina se verificó que los paquetes y bibliotecas estuvieran instalados. En el caso de ***python***, ***pip*** y ***MySQL*** con al commando *- -version*, para ***Django*** se puede ejecutar en el shell de python el siguiente script:

    ```sh
    import django
    django.VERSION
    ```
    Para el resto se pueden consultar en los servicion en *etc/init.d*
    
 - Se configuró el archivo ***httpd.config*** de apache para que suportara *cgi scripts* y se creó un script de python en la dirección *var/www/*.

## Problemas encontrados.

El primer problema encontrado fue el aprender a usar Buildroot ya que, aunque su finalidad es ser fácil de usar, ya espera que estes familiarizado con sistemas embebidos, en caso contrario a pesar de poder hacer un el compilado queda un abismo de incertidumbre sobre lo que se está haciendo en el momento de la configuración y lo que está haciendo Buildroot en el compilado. En otras palabras se siente poco demostrativo o poco claro el proceso. Sin embargo pues es posible hacer un compilado.

El segundo y último problema fue el ultimo punto del proceso ya que tuve problemas al hacer la conexión con apache y debido a falta de tiempo y conocimiento no me fue posible reolverlo, por lo que no pude probar el funcionamiento del sript.

## Acceso

Al momento de arrancar la máquina virtual, cuando se está iniciando syslog-ng es algo tardado y eventualmente hay que teclear cualquier cosa para que avance. Deapué, cuando ya es el login solo con entrar como **root** y se accede, aunque para ver las carpetar hast que hacer *cd /*

**Miguel Ángel Escamilla Monroy** 
